#!/usr/bin/python3

import cld3, sys

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    res = cld3.get_language(line)
    if res.language == "cs":
        print("%s\tces" % (line))
    elif res.language == "en":
        print("%s\teng" % (line))
    elif res.language == "de":
        print("%s\tdeu" % (line))
    elif res.language == "la":
        print("%s\tlat" % (line))
    else:
        print("%s\tunk" % (line))