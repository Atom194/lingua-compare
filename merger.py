#!/usr/bin/python3

import sys

files = []

for arg in sys.argv[1:]:
    files.append(open(arg, "r"))

do_break = False
while True:
    for i in range(0, len(files)):
        line = files[i].readline()
        if i == 0 and len(line) == 0:
            do_break = True
        print("%s" % line, end="")
    if do_break:
        break

for f in files:
    f.close()