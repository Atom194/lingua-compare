#!/usr/bin/python3

import nltk, sys


try:
    tc = nltk.classify.textcat.TextCat()
except:
    nltk.download('crubadan')
    nltk.download('punkt')
    tc = nltk.classify.textcat.TextCat()

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    res = tc.guess_language(line)
    print(res)
    continue
    if res[2][0][0] == "ces":
        print("%s\tces" % (line))
    elif res == "eng":
        print("%s\teng" % (line))
    elif res == "deu":
        print("%s\tdeu" % (line))
    elif res[2][0][0] == "lat":
        print("%s\tlat" % (line))
    else:
        print("%s\tunk" % (line))
