use lingua::{Language, LanguageDetector, LanguageDetectorBuilder};
use lingua::Language::{English, Czech, Latin, Dutch};
use std::io::{self, BufRead};

fn main() {
    let languages = vec![English, Czech, Latin, Dutch];
    let detector: LanguageDetector = LanguageDetectorBuilder::from_languages(&languages).build();
    let mut detected_language: Option<Language>;

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let ll = line.unwrap();
        detected_language = detector.detect_language_of(ll.clone());
        if detected_language == Some(English)
        {
            println!("{0}\teng", ll.clone());
        }
        else if detected_language == Some(Czech)
        {
            println!("{0}\tces", ll.clone());
        }
        else if detected_language == Some(Latin)
        {
            println!("{0}\tlat", ll.clone());
        }
        else if detected_language == Some(Dutch)
        {
            println!("{0}\tdeu", ll.clone());
        }
        else
        {
            println!("{0}\tunk", ll.clone());
        }
        
    }
}
