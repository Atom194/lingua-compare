#!/usr/bin/python3

import sys

for line in sys.stdin:
    if line[-1] == "\n":
        line = line[:-1]
    line = line.split("\t")
    if len(line[0]) > 3:
        print("%s\t%s" % (line[0], line[1]))
